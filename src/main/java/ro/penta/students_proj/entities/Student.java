package ro.penta.students_proj.entities;

import java.util.HashMap;
import java.util.Map;

public class Student {

	String name;
	int year;
	Map<String, Grade> grades = new HashMap<String, Grade>();
	
	public void setGrades(Map<String, Grade> grades) {
		this.grades = grades;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public Map<String, Grade> getGrades() {
		return grades;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		//return super.toString();
		String str = "name: "+name+", grades: [";
		for(Map.Entry<String,Grade> grade: grades.entrySet()) {
			str+= grade+" ";
		}
		str+="]";
		return str;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	public int getYear() {
		return year;
	}
}
