package ro.penta.students_proj.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ro.penta.students_proj.entities.Grade;
import ro.penta.students_proj.entities.Student;

public class StatisticServer {
	
	public void add(Set<String> rows, List<Student> students, String name, int year, String subName, int value) {
		try {
			FileWriter out = new FileWriter("src\\main\\res\\data.txt");
			BufferedWriter writer = new BufferedWriter(out);
			for(String row: rows){
				writer.write(row);
				writer.newLine();
			}
			Grade grade = new Grade();
			grade.setSubject(subName);
			grade.setGradeValue(value);
			int ok=0;
			for(Student student: students) {
				if(student.getName() == name) {
					student.getGrades().put(student.getName()+student.getGrades().size(), grade);
					ok=1;
				}
			}
			if(ok==0) {
				Student student = new Student();
				student.setName(name);
				student.setYear(year);
				Map<String, Grade> grades = new HashMap<>();
				grades.put(name+"1", grade);
				student.setGrades(grades);
				students.add(student);
			}
			String row = name+" "+year+" "+subName+" "+value;
			rows.add(row);
			writer.write(row);
			//iterate(rows);
			
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void iterate(Set<String> rows) {
		Iterator<String> iterator = rows.iterator();
		while (iterator.hasNext()) {
			String str = iterator.next();
			System.out.println(str);
		}
	}
	
	public void viewGrades(Set<String> rows, String subjectStatistic) {
		for(String mRow : rows) {
			if(mRow.contains(subjectStatistic)) {
				String[] val = mRow.split(" ");
				System.out.println("Student: "+val[0]+", grade: "+val[3]);
			}
		}
	}
	
	public void viewAverage(Set<String> rows, String avg) {
		double sum=0, count=0;
		for(String mRow : rows) {
			if(mRow.contains(avg)) {
				String[] val = mRow.split(" ");
				sum+= Integer.parseInt(val[3]);
				count++;
			}
			
		}
		double average = sum/count;
		System.out.println("Result: "+average);
	}

}
