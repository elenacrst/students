package ro.penta.students_proj;

import java.io.BufferedWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import ro.penta.students_proj.entities.Grade;
import ro.penta.students_proj.entities.Student;
import ro.penta.students_proj.services.StatisticServer;



/**
 * Hello world!
 *
 */
public class App 
{
	static File file = new File("src\\main\\res\\data.txt");
	static List<Student> students = new ArrayList<>();
	static Set<String> rows=new HashSet<>();
	
    public static void main( String[] args )
    {
    	System.out.println("Choose activity: add/ view/ statistics");
		Scanner scannerConsole = new Scanner(System.in);
		String option = scannerConsole.nextLine();
		
		read();
		
		StatisticServer statisticServer = new StatisticServer();
		
		switch(option) {
		case "add":
			System.out.println("Student's name: ");
			String name = scannerConsole.nextLine();
			System.out.println("Student's year: ");
			int year = Integer.parseInt(scannerConsole.nextLine());
			System.out.println("Subject's name: ");
			String subName = scannerConsole.nextLine();
			System.out.println("Grade's value: ");
			int value = Integer.parseInt(scannerConsole.nextLine());
			statisticServer.add(rows, students, name, year, subName, value);
			read();
			statisticServer.iterate(rows);
			break;
			
		case "view":
			statisticServer.iterate(rows);
			break;
			
		case "statistics":
			System.out.println("Enter 1 to view all grades for a specific"
					+ " subject, 2 to view average grade for a specific"
					+ " student, 3 to view average grade for a specific "
					+"subject");
			String statistic = scannerConsole.nextLine();
			if(statistic.contains("1")) {
				System.out.println("Subject's name: ");
				String subjectStatistic = scannerConsole.nextLine();
				statisticServer.viewGrades(rows, subjectStatistic);
				
			}else if(statistic.contains("2")) {
				System.out.println("Student's name: ");
				String avg = scannerConsole.nextLine();
				statisticServer.viewAverage(rows, avg);
				
			}else if(statistic.contains("3")) {
				System.out.println("Subject's name: ");
				String avg = scannerConsole.nextLine();
				/*double sum=0, count=0;
				for(String mRow : rows) {
					if(mRow.contains(avgSubject)) {
						String[] val = mRow.split(" ");
						sum+= Integer.parseInt(val[6]);
						count++;
					}
					
				}
				double avg = sum/count;
				System.out.println("Result: "+avg);
				*/
				statisticServer.viewAverage(rows, avg);
			}
				
			break;
		}
    	
    	
    }
    
    public static void read() {
    	try {
			Scanner studentsInfo = new Scanner(file);
			while(studentsInfo.hasNext()) {
				String row = studentsInfo.nextLine();
				rows.add(row);
				String[] values = row.split(" ");
				Student currentStudent = new Student();
				String name = values[0];
				currentStudent.setName(name);
				currentStudent.setYear(Integer.parseInt(values[1]));
				Grade grade = new Grade();
				String subject = values[2];
				grade.setSubject(subject);
				int gradeValue = Integer.valueOf(values[3]);
				grade.setGradeValue(gradeValue);
				int ok=0;
				for(Student student: students) {
					if(student.getName() == name) {
						student.getGrades().put(name+student.getGrades().size(), grade);
						ok=1;
					}				
				}
				if(ok==0) {
					currentStudent.getGrades().put(name+currentStudent.getGrades().size(), grade);
					students.add(currentStudent);
				}	
		
			}	
			studentsInfo.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
}
