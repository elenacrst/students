package ro.penta.students_proj.entities;

public class Grade {

	int gradeValue;
	String subject;
	
	public int getGradeValue() {
		return gradeValue;
	}
	public String getSubject() {
		return subject;
	}
	public void setGradeValue(int gradeValue) {
		this.gradeValue = gradeValue;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		//return super.toString();
		return "gradeValue: "+gradeValue+", subject: "+subject;
	}
}
